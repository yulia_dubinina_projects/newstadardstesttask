--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-06-13 08:31:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 197 (class 1259 OID 16529)
-- Name: album_by_band_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE album_by_band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE album_by_band_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16540)
-- Name: album_by_band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE album_by_band (
    id bigint DEFAULT nextval('album_by_band_id_seq'::regclass) NOT NULL,
    title text NOT NULL,
    band_id bigint NOT NULL
);


ALTER TABLE album_by_band OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16527)
-- Name: album_by_person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE album_by_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE album_by_person_id_seq OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16512)
-- Name: album_by_person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE album_by_person (
    id bigint DEFAULT nextval('album_by_person_id_seq'::regclass) NOT NULL,
    title text NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE album_by_person OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16491)
-- Name: band_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE band_id_seq OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16482)
-- Name: band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE band (
    id bigint DEFAULT nextval('band_id_seq'::regclass) NOT NULL,
    name text NOT NULL
);


ALTER TABLE band OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16531)
-- Name: collection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE collection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE collection_id_seq OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16608)
-- Name: collection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE collection (
    id bigint DEFAULT nextval('collection_id_seq'::regclass) NOT NULL,
    title text NOT NULL
);


ALTER TABLE collection OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16476)
-- Name: composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE composition_id_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16443)
-- Name: composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE composition (
    id bigint DEFAULT nextval('composition_id_seq'::regclass) NOT NULL,
    title text NOT NULL,
    author_id bigint NOT NULL,
    composer_id bigint NOT NULL
);


ALTER TABLE composition OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16478)
-- Name: performance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE performance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE performance_id_seq OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16464)
-- Name: performance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performance (
    id bigint DEFAULT nextval('performance_id_seq'::regclass) NOT NULL,
    perf_date date NOT NULL,
    composition_id bigint NOT NULL,
    single boolean NOT NULL
);


ALTER TABLE performance OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16555)
-- Name: performance_in_album_by_band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performance_in_album_by_band (
    performance_id bigint NOT NULL,
    album_id bigint NOT NULL
);


ALTER TABLE performance_in_album_by_band OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16577)
-- Name: performance_in_album_by_person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performance_in_album_by_person (
    performance_id bigint NOT NULL,
    album_id bigint NOT NULL
);


ALTER TABLE performance_in_album_by_person OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16633)
-- Name: performance_in_collection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performance_in_collection (
    performance_id bigint NOT NULL,
    collection_id bigint NOT NULL
);


ALTER TABLE performance_in_collection OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16666)
-- Name: performed_by_band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performed_by_band (
    performance_id bigint NOT NULL,
    band_id bigint NOT NULL
);


ALTER TABLE performed_by_band OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16649)
-- Name: performed_by_person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE performed_by_person (
    performance_id bigint NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE performed_by_person OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16402)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE person_id_seq OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16407)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE person (
    id bigint DEFAULT nextval('person_id_seq'::regclass) NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL
);


ALTER TABLE person OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16509)
-- Name: person_in_band_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE person_in_band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE person_in_band_id_seq OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16494)
-- Name: person_in_band; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE person_in_band (
    id bigint DEFAULT nextval('person_in_band_id_seq'::regclass) NOT NULL,
    person_id bigint NOT NULL,
    band_id bigint NOT NULL,
    date_from date NOT NULL,
    date_to date
);


ALTER TABLE person_in_band OWNER TO postgres;

--
-- TOC entry 2099 (class 2606 OID 16548)
-- Name: album_by_band album_by_band_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_by_band
    ADD CONSTRAINT album_by_band_pkey PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 16519)
-- Name: album_by_person album_by_person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_by_person
    ADD CONSTRAINT album_by_person_pkey PRIMARY KEY (id);


--
-- TOC entry 2091 (class 2606 OID 16489)
-- Name: band band_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY band
    ADD CONSTRAINT band_id_pkey PRIMARY KEY (id);


--
-- TOC entry 2108 (class 2606 OID 16616)
-- Name: collection collection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 16450)
-- Name: composition composition_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY composition
    ADD CONSTRAINT composition_id_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 16468)
-- Name: performance performance_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance
    ADD CONSTRAINT performance_id_pkey PRIMARY KEY (id);


--
-- TOC entry 2101 (class 2606 OID 16559)
-- Name: performance_in_album_by_band performance_in_album_by_band_composite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_band
    ADD CONSTRAINT performance_in_album_by_band_composite_pkey PRIMARY KEY (album_id, performance_id);


--
-- TOC entry 2104 (class 2606 OID 16581)
-- Name: performance_in_album_by_person performance_in_album_by_person_composite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_person
    ADD CONSTRAINT performance_in_album_by_person_composite_pkey PRIMARY KEY (album_id, performance_id);


--
-- TOC entry 2110 (class 2606 OID 16637)
-- Name: performance_in_collection performance_in_collection_composite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_collection
    ADD CONSTRAINT performance_in_collection_composite_pkey PRIMARY KEY (collection_id, performance_id);


--
-- TOC entry 2117 (class 2606 OID 16670)
-- Name: performed_by_band performed_by_band_composite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_band
    ADD CONSTRAINT performed_by_band_composite_pkey PRIMARY KEY (band_id, performance_id);


--
-- TOC entry 2113 (class 2606 OID 16653)
-- Name: performed_by_person performed_by_person_composite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_person
    ADD CONSTRAINT performed_by_person_composite_pkey PRIMARY KEY (person_id, performance_id);


--
-- TOC entry 2079 (class 2606 OID 16415)
-- Name: person person_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person
    ADD CONSTRAINT person_id_pkey PRIMARY KEY (id);


--
-- TOC entry 2093 (class 2606 OID 16498)
-- Name: person_in_band person_in_band_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person_in_band
    ADD CONSTRAINT person_in_band_id_pkey PRIMARY KEY (id);


--
-- TOC entry 2097 (class 1259 OID 16554)
-- Name: album_by_band_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX album_by_band_id_idx ON album_by_band USING btree (id);


--
-- TOC entry 2094 (class 1259 OID 16526)
-- Name: album_by_person_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX album_by_person_id_idx ON album_by_person USING btree (id);


--
-- TOC entry 2089 (class 1259 OID 16490)
-- Name: band_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX band_id_idx ON band USING btree (id);


--
-- TOC entry 2106 (class 1259 OID 16617)
-- Name: collection_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX collection_id_idx ON collection USING btree (id);


--
-- TOC entry 2080 (class 1259 OID 16461)
-- Name: composition_author_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX composition_author_id_idx ON composition USING btree (author_id);


--
-- TOC entry 2081 (class 1259 OID 16462)
-- Name: composition_composer_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX composition_composer_id_idx ON composition USING btree (composer_id);


--
-- TOC entry 2082 (class 1259 OID 16463)
-- Name: composition_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX composition_id_idx ON composition USING btree (id);


--
-- TOC entry 2085 (class 1259 OID 16475)
-- Name: performance_composition_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX performance_composition_id_idx ON performance USING btree (composition_id);


--
-- TOC entry 2086 (class 1259 OID 16474)
-- Name: performance_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performance_id_idx ON performance USING btree (id);


--
-- TOC entry 2102 (class 1259 OID 16576)
-- Name: performance_in_album_by_band_composite_pkey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performance_in_album_by_band_composite_pkey_idx ON performance_in_album_by_band USING btree (performance_id, album_id);


--
-- TOC entry 2105 (class 1259 OID 16592)
-- Name: performance_in_album_by_person_composite_pkey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performance_in_album_by_person_composite_pkey_idx ON performance_in_album_by_person USING btree (performance_id, album_id);


--
-- TOC entry 2111 (class 1259 OID 16648)
-- Name: performance_in_collection_composite_pkey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performance_in_collection_composite_pkey_idx ON performance_in_collection USING btree (performance_id, collection_id);


--
-- TOC entry 2118 (class 1259 OID 16681)
-- Name: performed_by_band_composite_pkey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performed_by_band_composite_pkey_idx ON performed_by_band USING btree (band_id, performance_id);


--
-- TOC entry 2119 (class 1259 OID 16682)
-- Name: performed_by_band_performance_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX performed_by_band_performance_id_idx ON performed_by_band USING btree (performance_id);


--
-- TOC entry 2114 (class 1259 OID 16664)
-- Name: performed_by_person_composite_pkey_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX performed_by_person_composite_pkey_idx ON performed_by_person USING btree (person_id, performance_id);


--
-- TOC entry 2115 (class 1259 OID 16665)
-- Name: performed_by_person_performance_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX performed_by_person_performance_id_idx ON performed_by_person USING btree (performance_id);


--
-- TOC entry 2077 (class 1259 OID 16416)
-- Name: person_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX person_id_idx ON person USING btree (id);


--
-- TOC entry 2126 (class 2606 OID 16549)
-- Name: album_by_band album_by_band_band_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_by_band
    ADD CONSTRAINT album_by_band_band_id_fkey FOREIGN KEY (band_id) REFERENCES band(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2125 (class 2606 OID 16520)
-- Name: album_by_person album_by_person_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY album_by_person
    ADD CONSTRAINT album_by_person_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2120 (class 2606 OID 16451)
-- Name: composition compostion_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY composition
    ADD CONSTRAINT compostion_author_id_fkey FOREIGN KEY (author_id) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2121 (class 2606 OID 16456)
-- Name: composition compostion_composer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY composition
    ADD CONSTRAINT compostion_composer_id_fkey FOREIGN KEY (composer_id) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2122 (class 2606 OID 16469)
-- Name: performance performance_composition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance
    ADD CONSTRAINT performance_composition_id_fkey FOREIGN KEY (composition_id) REFERENCES composition(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2127 (class 2606 OID 16560)
-- Name: performance_in_album_by_band performance_in_album_by_band_album_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_band
    ADD CONSTRAINT performance_in_album_by_band_album_id_fkey FOREIGN KEY (album_id) REFERENCES album_by_band(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2128 (class 2606 OID 16565)
-- Name: performance_in_album_by_band performance_in_album_by_band_performance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_band
    ADD CONSTRAINT performance_in_album_by_band_performance_id_fkey FOREIGN KEY (performance_id) REFERENCES performance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2129 (class 2606 OID 16582)
-- Name: performance_in_album_by_person performance_in_album_by_person_album_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_person
    ADD CONSTRAINT performance_in_album_by_person_album_id_fkey FOREIGN KEY (album_id) REFERENCES album_by_person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2130 (class 2606 OID 16587)
-- Name: performance_in_album_by_person performance_in_album_by_person_performance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_album_by_person
    ADD CONSTRAINT performance_in_album_by_person_performance_id_fkey FOREIGN KEY (performance_id) REFERENCES performance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2131 (class 2606 OID 16638)
-- Name: performance_in_collection performance_in_collection_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_collection
    ADD CONSTRAINT performance_in_collection_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2132 (class 2606 OID 16643)
-- Name: performance_in_collection performance_in_collection_performance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performance_in_collection
    ADD CONSTRAINT performance_in_collection_performance_id_fkey FOREIGN KEY (performance_id) REFERENCES performance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2135 (class 2606 OID 16671)
-- Name: performed_by_band performed_by_band_band_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_band
    ADD CONSTRAINT performed_by_band_band_id_fkey FOREIGN KEY (band_id) REFERENCES band(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2136 (class 2606 OID 16676)
-- Name: performed_by_band performed_by_band_performance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_band
    ADD CONSTRAINT performed_by_band_performance_id_fkey FOREIGN KEY (performance_id) REFERENCES performance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2134 (class 2606 OID 16659)
-- Name: performed_by_person performed_by_person_performance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_person
    ADD CONSTRAINT performed_by_person_performance_id_fkey FOREIGN KEY (performance_id) REFERENCES performance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2133 (class 2606 OID 16654)
-- Name: performed_by_person performed_by_person_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY performed_by_person
    ADD CONSTRAINT performed_by_person_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2124 (class 2606 OID 16504)
-- Name: person_in_band person_in_band_band_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person_in_band
    ADD CONSTRAINT person_in_band_band_id_fkey FOREIGN KEY (band_id) REFERENCES band(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2123 (class 2606 OID 16499)
-- Name: person_in_band person_in_band_person_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY person_in_band
    ADD CONSTRAINT person_in_band_person_id_fkey FOREIGN KEY (person_id) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2017-06-13 08:31:11

--
-- PostgreSQL database dump complete
--


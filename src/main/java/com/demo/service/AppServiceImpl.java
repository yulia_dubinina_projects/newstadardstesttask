package com.demo.service;

import com.demo.dao.AppDao;
import com.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dhdaan on 11.06.2017.
 */
@Service("appService")
public class AppServiceImpl implements AppService {

    @Autowired
    AppDao appDao;

    @Override
    public void cleanTheDatabase() {
        appDao.cleanTheDatabase();
    }

    @Override
    public void insertPerson(Person person) {
        appDao.insertPerson(person);
    }

    @Override
    public void insertComposition(Composition composition) {
        appDao.insertComposition(composition);
    }

    @Override
    public void insertPerformance(Performance performance) {
        appDao.insertPerformance(performance);
    }

    @Override
    public void insertCollection(Collection collection) {
        appDao.insertCollection(collection);
    }

    @Override
    public void insertBand(Band band) {
        appDao.insertBand(band);
    }

    @Override
    public List<Composition> getAllCompositions() {
        return appDao.getAllCompositions();
    }

    @Override
    public List<Performer> getPerformersByCompositionId(long id) {
        List<Long> performanceIds = appDao.getPerformanceIdsByCompositionId(id);
        List<Performer> result = new ArrayList<>();
        result.addAll(appDao.getPersonsByPerformanceIds(performanceIds));
        result.addAll(appDao.getBandsByPerformanceIds(performanceIds));
        return result;
    }

    @Override
    public List<Band> getBandsByPersonId(long id) {
        return appDao.getBandsByPersonId(id);
    }

    @Override
    public List<Collection> getCollectionsByCompositionId(long id) {
        List<Long> performanceIds = appDao.getPerformanceIdsByCompositionId(id);
        List<Collection> result = new ArrayList<>();
        result.addAll(appDao.getCollectionsByPerformanceIds(performanceIds));
        result.addAll(appDao.getAlbumsByBandByPerformanceIds(performanceIds));
        result.addAll(appDao.getAlbumsByPersonByPerformanceIds(performanceIds));
        return result;
    }

    @Override
    public List<Person> getAllComposers() {
        return appDao.getAllComposers();
    }

    @Override
    public List<Person> getAllAuthors() {
        return appDao.getAllAuthors();
    }

    @Override
    public List<Composition> getCompositionsByComposerId(long id) {
        return appDao.getCompositionsByComposerId(id);
    }

    @Override
    public List<Composition> getCompositionsByAuthorId(long id) {
        return appDao.getCompositionsByAuthorId(id);
    }

    @Override
    public List<Performer> getAllPerformers() {
        List<Performer> result = new ArrayList<>();
        result.addAll(appDao.getAllPersonPerformers());
        result.addAll(appDao.getAllBands());
        return result;
    }

    @Override
    public List<Composition> getCompositionsByPerformer(long id, String type) {
        if (type.equals(Band.class.getSimpleName())) {
            return appDao.getCompositionsByBandId(id);
        }
        return appDao.getCompositionsByPersonId(id);
    }

    @Override
    public List<Person> getAllPersonPerformers() {
        return appDao.getAllPersonPerformers();
    }
}

package com.demo.service;

import com.demo.model.*;

import java.util.List;

/**
 * Created by dhdaan on 11.06.2017.
 */
public interface AppService {

    public void cleanTheDatabase();

    public void insertPerson(Person person);

    public void insertComposition(Composition composition);

    public void insertPerformance(Performance performance);

    public void insertCollection(Collection collection);

    public void insertBand(Band band);

    public List<Composition> getAllCompositions();

    public List<Performer> getPerformersByCompositionId(long id);

    public List<Band> getBandsByPersonId(long id);

    public List<Collection> getCollectionsByCompositionId(long id);

    public List<Person> getAllComposers();

    public List<Person> getAllAuthors();

    public List<Composition> getCompositionsByComposerId(long id);

    public List<Composition> getCompositionsByAuthorId(long id);

    public List<Performer> getAllPerformers();

    public List<Composition> getCompositionsByPerformer(long id, String type);

    public List<Person> getAllPersonPerformers();
}

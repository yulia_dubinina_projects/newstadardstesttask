package com.demo.dao;

import com.demo.model.*;

import java.util.List;

/**
 * Created by dhdaan on 11.06.2017.
 */
public interface AppDao {

    public void cleanTheDatabase();

    public void insertPerson(Person person);

    public void insertComposition(Composition composition);

    public void insertPerformance(Performance performance);

    public void insertCollection(Collection collection);

    public void insertBand(Band band);

    public List<Composition> getAllCompositions();

    public List<Long> getPerformanceIdsByCompositionId(long id);

    public List<Person> getPersonsByPerformanceIds(List<Long> performanceIds);

    public List<Band> getBandsByPerformanceIds(List<Long> performanceIds);

    public List<Band> getBandsByPersonId(long id);

    public List<Collection> getCollectionsByPerformanceIds(List<Long> performanceIds);

    public List<Album> getAlbumsByBandByPerformanceIds(List<Long> performanceIds);

    public List<Album> getAlbumsByPersonByPerformanceIds(List<Long> performanceIds);

    public List<Person> getAllComposers();

    public List<Person> getAllAuthors();

    public List<Composition> getCompositionsByAuthorId(long id);

    public List<Composition> getCompositionsByComposerId(long id);

    public List<Person> getAllPersonPerformers();

    public List<Band> getAllBands();

    public List<Composition> getCompositionsByBandId(long id);

    public List<Composition> getCompositionsByPersonId(long id);
}

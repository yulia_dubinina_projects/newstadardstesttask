package com.demo.dao;

import com.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dhdaan on 11.06.2017.
 */
@Repository
@Qualifier("appDao")
public class AppDaoImpl implements AppDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private List<String> getTableNames() {
        List<String> names = jdbcTemplate.query("SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = 'public'", new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString(1);
            }
        });
        return names;
    }

    @Override
    public void cleanTheDatabase() {
        for (String name : getTableNames()) {
            String sql = "truncate table " + name + " cascade";
            System.out.println("EXECUTING: " + sql);
            jdbcTemplate.update(sql);
        }
    }

    @Override
    public void insertPerson(Person person) {
        person.setId(jdbcTemplate.execute("insert into person (first_name,last_name) values (?,?) returning id",
                new PreparedStatementCallback<Long>() {
                    public Long doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {

                        preparedStatement.setString(1, person.getFirstName());
                        preparedStatement.setString(2, person.getLastName());
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()) {
                            return rs.getLong(1);
                        }
                        return -1l;
                    }
                })
        );
    }

    @Override
    public void insertComposition(Composition composition) {

        composition.setId(jdbcTemplate.execute("insert into composition (title,author_id,composer_id) values (?,?,?) returning id",
                new PreparedStatementCallback<Long>()

                {
                    public Long doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {

                        preparedStatement.setString(1, composition.getTitle());
                        preparedStatement.setLong(2, composition.getAuthor().getId());
                        preparedStatement.setLong(3, composition.getComposer().getId());
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()) {
                            return rs.getLong(1);
                        }
                        return -1l;
                    }
                })
        );
    }

    @Override
    public void insertPerformance(Performance performance) {
        performance.setId(jdbcTemplate.execute("insert into performance (perf_date,composition_id,single) values (?,?,?) returning id",
                new PreparedStatementCallback<Long>()

                {
                    public Long doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {

                        preparedStatement.setDate(1, Date.valueOf(performance.getDate()));
                        preparedStatement.setLong(2, performance.getComposition().getId());
                        preparedStatement.setBoolean(3, performance.isSingle());
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()) {
                            return rs.getLong(1);
                        }
                        return -1l;
                    }
                })
        );
        for (Performer performer : performance.getPerformers()) {
            final String name;
            if (performer instanceof Person) {
                name = "person";
            } else {
                name = "band";
            }
            jdbcTemplate.update("insert into performed_by_" + name + " (performance_id," + name + "_id) values (?,?)",
                    new PreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement preparedStatement) throws SQLException {
                            preparedStatement.setLong(1, performance.getId());
                            preparedStatement.setLong(2, performer.getId());
                        }
                    }
            );
        }
    }

    @Override
    public void insertCollection(Collection collection) {
        final long performerId;
        final String collectionTableName;
        final String performanceTableName;
        final String collectionIdField;
        final String performerIdField;
        if (collection instanceof Album) {
            Album album = (Album) collection;
            performerId = album.getPerformer().getId();
            if (album.getPerformer() instanceof Person) {
                performanceTableName = "performance_in_album_by_person";
                performerIdField = "person_id";
                collectionTableName = "album_by_person";
                collectionIdField = "album_id";
            } else {
                performanceTableName = "performance_in_album_by_band";
                performerIdField = "band_id";
                collectionTableName = "album_by_band";
                collectionIdField = "album_id";
            }
        } else {
            performerId = -1;
            performanceTableName = "performance_in_collection";
            performerIdField = null;
            collectionTableName = "collection";
            collectionIdField = "collection_id";
        }
        collection.setId(jdbcTemplate.execute("insert into " + collectionTableName +
                        " (title" + (performerIdField != null ? ("," + performerIdField) : "") + ")" +
                        " values (?" + (performerIdField != null ? (",?") : "") + ") returning id",
                new PreparedStatementCallback<Long>() {
                    public Long doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {

                        preparedStatement.setString(1, collection.getTitle());
                        if (performerIdField != null) {
                            preparedStatement.setLong(2, performerId);
                        }
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()) {
                            return rs.getLong(1);
                        }
                        return -1l;
                    }
                })
        );
        for (Performance performance : collection.getPerformances()) {
            jdbcTemplate.update("insert into " + performanceTableName + " (performance_id," + collectionIdField + ") values (?,?)",
                    new PreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement preparedStatement) throws SQLException {
                            preparedStatement.setLong(1, performance.getId());
                            preparedStatement.setLong(2, collection.getId());
                        }
                    }
            );
        }
    }

    @Override
    public void insertBand(Band band) {
        band.setId(jdbcTemplate.execute("insert into band (name) values (?) returning id",
                new PreparedStatementCallback<Long>()

                {
                    public Long doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {

                        preparedStatement.setString(1, band.getName());
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()) {
                            return rs.getLong(1);
                        }
                        return -1l;
                    }
                })
        );
        for (Map.Entry<Person, List<DateRange>> entry : band.getPersons().entrySet()) {
            for (DateRange dr : entry.getValue()) {
                jdbcTemplate.update("insert into person_in_band (person_id,band_id,date_from,date_to) values (?,?,?,?)",
                        new PreparedStatementSetter() {
                            @Override
                            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                                preparedStatement.setLong(1, entry.getKey().getId());
                                preparedStatement.setLong(2, band.getId());
                                preparedStatement.setDate(3, Date.valueOf(dr.getDateFrom()));
                                preparedStatement.setDate(4, dr.getDateTo() == null ? null : Date.valueOf(dr.getDateTo()));
                            }
                        }
                );
            }
        }
    }

    @Override
    public List<Composition> getAllCompositions() {
        return jdbcTemplate.query("SELECT c.id,c.title,p1.id,p1.first_name,p1.last_name,p2.id,p2.first_name,p2.last_name FROM person p1, person p2, composition c where c.author_id=p1.id and c.composer_id=p2.id", new RowMapper<Composition>() {
            @Override
            public Composition mapRow(ResultSet rs, int rowNum) throws SQLException {
                Person author = new Person(rs.getLong(3), rs.getString(4), rs.getString(5));
                Person composer = new Person(rs.getLong(6), rs.getString(7), rs.getString(8));
                return new Composition(rs.getLong(1), rs.getString(2), author, composer);
            }
        });
    }

    @Override
    public List<Long> getPerformanceIdsByCompositionId(final long id) {
        return jdbcTemplate.execute("SELECT id from performance where composition_id=?",
                new PreparedStatementCallback<List<Long>>() {
                    public List<Long> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        preparedStatement.setLong(1, id);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Long> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(rs.getLong(1));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Person> getPersonsByPerformanceIds(final List<Long> performanceIds) {
        return jdbcTemplate.execute("SELECT p.id,p.first_name,p.last_name from person p where id in (" +
                        "select distinct person_id from performed_by_person r where r.performance_id=any(?) )",
                new PreparedStatementCallback<List<Person>>() {
                    public List<Person> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        Array arr = preparedStatement.getConnection().createArrayOf("bigint", performanceIds.toArray(new Long[0]));
                        preparedStatement.setArray(1, arr);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Person> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Person(rs.getLong(1), rs.getString(2), rs.getString(3)));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Band> getBandsByPerformanceIds(final List<Long> performanceIds) {
        return jdbcTemplate.execute("SELECT b.id,b.name from band b where id in (" +
                        "select distinct band_id from performed_by_band r where r.performance_id=any(?) )",
                new PreparedStatementCallback<List<Band>>() {
                    public List<Band> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        Array arr = preparedStatement.getConnection().createArrayOf("bigint", performanceIds.toArray(new Long[0]));
                        preparedStatement.setArray(1, arr);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Band> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Band(rs.getLong(1), rs.getString(2), null));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Band> getBandsByPersonId(final long id) {
        return jdbcTemplate.execute("SELECT b.id,b.name from band b where id in (" +
                        "select distinct band_id from person_in_band r where r.person_id=? )",
                new PreparedStatementCallback<List<Band>>() {
                    public List<Band> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        preparedStatement.setLong(1, id);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Band> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Band(rs.getLong(1), rs.getString(2), null));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Collection> getCollectionsByPerformanceIds(final List<Long> performanceIds) {
        return jdbcTemplate.execute("SELECT c.id,c.title from collection c where id in (" +
                        "select distinct collection_id from performance_in_collection r where r.performance_id=any(?) )",
                new PreparedStatementCallback<List<Collection>>() {
                    public List<Collection> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        Array arr = preparedStatement.getConnection().createArrayOf("bigint", performanceIds.toArray(new Long[0]));
                        preparedStatement.setArray(1, arr);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Collection> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Collection(rs.getLong(1), rs.getString(2), null));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Album> getAlbumsByBandByPerformanceIds(List<Long> performanceIds) {
        return jdbcTemplate.execute("SELECT c.id,c.title,b.id,b.name " +
                        "from album_by_band c, band b where b.id=c.band_id and c.id in (" +
                        "select distinct album_id from performance_in_album_by_band r where r.performance_id=any(?) )",
                new PreparedStatementCallback<List<Album>>() {
                    public List<Album> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        Array arr = preparedStatement.getConnection().createArrayOf("bigint", performanceIds.toArray(new Long[0]));
                        preparedStatement.setArray(1, arr);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Album> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Album(rs.getLong(1), rs.getString(2), null,
                                    new Band(rs.getLong(3), rs.getString(4), null)));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Album> getAlbumsByPersonByPerformanceIds(List<Long> performanceIds) {
        return jdbcTemplate.execute("SELECT c.id,c.title,p.id,p.first_name,p.last_name " +
                        "from album_by_person c, person p where c.person_id=p.id and c.id in (" +
                        "select distinct album_id from performance_in_album_by_person r where r.performance_id=any(?) )",
                new PreparedStatementCallback<List<Album>>() {
                    public List<Album> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        Array arr = preparedStatement.getConnection().createArrayOf("bigint", performanceIds.toArray(new Long[0]));
                        preparedStatement.setArray(1, arr);
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Album> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new Album(rs.getLong(1), rs.getString(2), null,
                                    new Person(rs.getLong(3), rs.getString(4), rs.getString(5))));
                        }
                        return result;
                    }
                });
    }

    @Override
    public List<Person> getAllComposers() {
        return jdbcTemplate.query("SELECT p.id,p.first_name,p.last_name FROM person p " +
                "where p.id in (select distinct composer_id from composition)", new RowMapper<Person>() {
            @Override
            public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Person(rs.getLong(1), rs.getString(2), rs.getString(3));
            }
        });
    }

    @Override
    public List<Person> getAllAuthors() {
        return jdbcTemplate.query("SELECT p.id,p.first_name,p.last_name FROM person p " +
                "where p.id in (select distinct author_id from composition)", new RowMapper<Person>() {
            @Override
            public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Person(rs.getLong(1), rs.getString(2), rs.getString(3));
            }
        });
    }

    @Override
    public List<Composition> getCompositionsByAuthorId(final long id) {
        return getCompositionsBySqlWithLongParams(
                "SELECT c.id,c.title,p1.id,p1.first_name,p1.last_name,p2.id,p2.first_name,p2.last_name " +
                        "FROM person p1, person p2, composition c where c.author_id=p1.id and c.composer_id=p2.id " +
                        "and c.author_id=?", id);
    }

    @Override
    public List<Composition> getCompositionsByComposerId(long id) {
        return getCompositionsBySqlWithLongParams(
                "SELECT c.id,c.title,p1.id,p1.first_name,p1.last_name,p2.id,p2.first_name,p2.last_name " +
                        "FROM person p1, person p2, composition c where c.author_id=p1.id and c.composer_id=p2.id " +
                        "and c.composer_id=?", id);
    }

    @Override
    public List<Person> getAllPersonPerformers() {
        return jdbcTemplate.query("SELECT p.id,p.first_name,p.last_name FROM person p " +
                "where p.id in ((select person_id from performed_by_person)UNION" +
                "(select person_id from person_in_band))", new RowMapper<Person>() {
            @Override
            public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Person(rs.getLong(1), rs.getString(2), rs.getString(3));
            }
        });
    }

    @Override
    public List<Band> getAllBands() {
        return jdbcTemplate.query("SELECT b.id,b.name FROM band b", new RowMapper<Band>() {
            @Override
            public Band mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new Band(rs.getLong(1), rs.getString(2), null);
            }
        });
    }

    @Override
    public List<Composition> getCompositionsByBandId(long id) {
        return getCompositionsBySqlWithLongParams(
                "SELECT c.id,c.title,p1.id,p1.first_name,p1.last_name,p2.id,p2.first_name,p2.last_name " +
                        "FROM person p1, person p2, composition c where c.author_id=p1.id and c.composer_id=p2.id " +
                        "and c.id in(select perf.composition_id from performance perf where perf.id in" +
                        "(select pb.performance_id from performed_by_band pb where pb.band_id=?) )", id);
    }

    //This is a complex query.
    //
    //A person can perform a composition directly without a band. This is the simplest case.
    //Performance will be associated directly with a person.
    //
    //Another case is when person is a not an independent performer. In this case he is a part of the band.
    //So the performances will be associated with bands that person was in, not with the person directly.
    //Moreover, for each found performance by band we have to check performance date, because a person might not
    //be a part of that band at the moment this performance was made.
    //E.g. not all Aria songs are displayed for Berkut and Kipelov
    @Override
    public List<Composition> getCompositionsByPersonId(long id) {
        //the easy part - direct relation
        String subQueryByPersons = "select perf_pers.performance_id from performed_by_person perf_pers " +
                "where perf_pers.person_id=?";

        //the hard part
        String tempTableBandsWithDateRanges = "(select pb.date_from,pb.date_to,pb.band_id from" +
                " person_in_band pb where person_id=?)as band_time";

        String timeCheck = "" +
                "(" +
                "  (band_time.date_to is NULL and perf_inner.perf_date>=band_time.date_from)" +
                "or" +
                "  (" +
                "     band_time.date_to is not NULL and " +
                "     (perf_inner.perf_date between band_time.date_from and band_time.date_to)" +
                "   ) " +
                ")";

        String subQueryByBands = "select perf_inner.id from performed_by_band perf_band, " +
                "performance perf_inner, " + tempTableBandsWithDateRanges +
                " where perf_band.band_id=band_time.band_id and perf_inner.id=perf_band.performance_id" +
                " and " + timeCheck;


        String sql = "SELECT c.id,c.title,p1.id,p1.first_name,p1.last_name,p2.id,p2.first_name,p2.last_name " +
                "FROM person p1, person p2, composition c where c.author_id=p1.id and c.composer_id=p2.id " +
                "and c.id in (" +
                "   select perf.composition_id from performance perf " +
                "   where perf.id in ((" + subQueryByBands + ") union (" + subQueryByPersons + "))" +
                ")";
        return getCompositionsBySqlWithLongParams(
                sql, id, id);
    }


    private List<Composition> getCompositionsBySqlWithLongParams(String sql, long... longParams) {
        return jdbcTemplate.execute(sql,
                new PreparedStatementCallback<List<Composition>>() {
                    public List<Composition> doInPreparedStatement(PreparedStatement preparedStatement) throws
                            SQLException, DataAccessException {
                        for (int i = 0; i < longParams.length; i++) {
                            preparedStatement.setLong(i + 1, longParams[i]);
                        }
                        ResultSet rs = preparedStatement.executeQuery();
                        List<Composition> result = new ArrayList<>();
                        while (rs.next()) {
                            Person author = new Person(rs.getLong(3), rs.getString(4), rs.getString(5));
                            Person composer = new Person(rs.getLong(6), rs.getString(7), rs.getString(8));
                            result.add(new Composition(rs.getLong(1), rs.getString(2), author, composer));
                        }
                        return result;
                    }
                });
    }
}
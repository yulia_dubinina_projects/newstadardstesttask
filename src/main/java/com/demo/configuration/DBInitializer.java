package com.demo.configuration;

import com.demo.model.*;
import com.demo.model.Collection;
import com.demo.service.AppService;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.*;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class DBInitializer {
    @Autowired
    AppService appService;

    private class PerformancesWithSamePerformersInsertionHelper {
        Composition[] compositions;
        LocalDate[] perfDates;
        boolean[] isSingle;
        List<Performer> performers;

        void init4album(Composition[] compositions, LocalDate defaultDate, Person person) {
            init(compositions, person);
            java.util.Arrays.fill(perfDates, defaultDate);
            java.util.Arrays.fill(isSingle, false);
        }

        void init4album(Composition[] compositions, LocalDate defaultDate, Band band) {
            init(compositions, band);
            java.util.Arrays.fill(perfDates, defaultDate);
            java.util.Arrays.fill(isSingle, false);
        }

        void init(Composition[] compositions) {
            this.compositions = compositions;
            perfDates = new LocalDate[compositions.length];
            isSingle = new boolean[compositions.length];
        }

        void init(Composition[] compositions, Person person) {
            init(compositions);
            performers = Arrays.asList(new Performer[]{person});
        }

        void init(Composition[] compositions, Band band) {
            init(compositions);
            performers = Arrays.asList(new Performer[]{band});
        }

        void markAsSingle(int ind, LocalDate date) {
            perfDates[ind] = date;
            isSingle[ind] = true;
        }

        List<Performance> uploadPerformances() {
            List<Performance> result = new ArrayList<>();
            for (int i = 0; i < compositions.length; i++) {
                Performance performance = new Performance(perfDates[i], compositions[i], isSingle[i], performers);
                result.add(performance);
                appService.insertPerformance(performance);
            }
            return result;
        }

        public Collection uploadAlbum(String title) {
            Collection collection = new Album(title, uploadPerformances(), performers.get(0));
            appService.insertCollection(collection);
            return collection;
        }
    }

    private void uploadPersons(Person... persons) {
        for (Person person : persons) {
            appService.insertPerson(person);
        }
    }

    private void sayTime(String msg, long time) {
        System.out.println(msg + " " + (System.currentTimeMillis() - time) + "ms");
    }

    @PostConstruct
    public void init() {
        long time = System.currentTimeMillis();
        long total = time;
        appService.cleanTheDatabase();
        List<Performance> forCollection1 = new ArrayList<Performance>();
        List<Performance> forCollection2 = new ArrayList<Performance>();
        PerformancesWithSamePerformersInsertionHelper perfInsHelper = new PerformancesWithSamePerformersInsertionHelper();
        sayTime("Cleaning the database", time);

        time = System.currentTimeMillis();
        uploadRelated2Zemfira(forCollection1, perfInsHelper);
        sayTime("Uploading objects related to Zemfira", time);

        time = System.currentTimeMillis();
        uploadRelated2Lagutenko(forCollection1, perfInsHelper);
        sayTime("Uploading objects related to Lagutenko", time);

        time = System.currentTimeMillis();
        uploadRelated2Lidemann(forCollection2, perfInsHelper);
        sayTime("Uploading objects related to Lidemann", time);

        time = System.currentTimeMillis();
        uploadRelated2SOAD(forCollection2, perfInsHelper);
        sayTime("Uploading objects related to SOAD", time);

        time = System.currentTimeMillis();
        uploadRelated2AgataKristy(forCollection1, perfInsHelper);
        sayTime("Uploading objects related to AgataKristy", time);

        time = System.currentTimeMillis();
        uploadRelated2Aria(forCollection1, perfInsHelper);
        sayTime("Uploading objects related to Aria", time);

        time = System.currentTimeMillis();
        uploadRelated2Trio();
        sayTime("Uploading objects related to Trio", time);

        time = System.currentTimeMillis();
        appService.insertCollection(new Collection("Популярное русское", forCollection1));
        appService.insertCollection(new Collection("Популярное иностранное", forCollection2));
        sayTime("Uploading collections", time);

        sayTime("TOTAL INIT DB", total);
    }

    private void uploadRelated2Trio() {
        //*******************Leps, Kobzon, Rozenbaum*******************
        Person leps = new Person("Григорий", "Лепс");//performer
        Person kobzon = new Person("Иосиф", "Кобзон");//composer,author,performer(kish)
        Person rosenbaum = new Person("Александр", "Розенбаум");//composer,author,performer
        uploadPersons(leps, kobzon, rosenbaum);
        //Special case: single with three person at the same time
        Composition[] compositions = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Вечерняя застольная",}, rosenbaum, rosenbaum);
        Performance performance = new Performance(LocalDate.of(2011, 10, 31), compositions[0], true, Arrays.asList(new Performer[]{leps, rosenbaum, kobzon}));
        appService.insertPerformance(performance);
    }

    private void uploadRelated2Aria(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person kipelov = new Person("Валерий", "Кипелов");
        Person dubinin = new Person("Виталий", "Дубинин");
        Person pushkina = new Person("Маргарита", "Пушкина");
        Person berkut = new Person("Артур", "Беркут");
        Person mavrin = new Person("Сергей", "Маврин");
        uploadPersons(kipelov, dubinin, pushkina, berkut, mavrin);
        //by Aria
        Band band = uploadBand(new Person[]{kipelov, dubinin, berkut, mavrin},
                new LocalDate[]{LocalDate.of(1985, 10, 31),
                        LocalDate.of(1987, 1, 1),
                        LocalDate.of(2002, 11, 9),
                        LocalDate.of(1987, 1, 1)},
                new LocalDate[]{LocalDate.of(2002, 8, 31),
                        null,
                        LocalDate.of(2011, 8, 31),
                        LocalDate.of(1995, 1, 31)}, "Ария");
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Паранойя",
                "Уходи и не возвращайся",
                "Дух войны"
        }, pushkina, dubinin), LocalDate.of(1995, 9, 11), band);
        Collection album = perfInsHelper.uploadAlbum("Ночь короче дня");
        forCollection1.add(album.getPerformances().get(2));
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Крещение огнём",
                "Твой новый мир",
                "Там высоко",
                "Бал у Князя Тьмы"
        }, pushkina, dubinin), LocalDate.of(2003, 5, 29), band);
        perfInsHelper.uploadAlbum("Крещение огнём");
        //by Kipelov/Mavrin
        LocalDate date1 = LocalDate.of(1997, 1, 1);
        LocalDate date2 = LocalDate.of(1997, 12, 31);
        band = uploadBand(new Person[]{kipelov, mavrin}, new LocalDate[]{date1, date1}, new LocalDate[]{date2, date2}, "Кипелов/Маврин");
        Composition[] c1 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Путь наверх",
                "Смутное время",
                "Ночь в июле"
        }, pushkina, kipelov);
        //Special case: nightInJune - the same composition performed by different performers in different time
        //(see usages below)
        Composition nightInJune = c1[2];
        Composition[] c2 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Свет дневной иссяк...",
                "Castlevania"
        }, pushkina, mavrin);
        perfInsHelper.init4album((Composition[]) ArrayUtils.addAll(c1, c2), LocalDate.of(1997, 9, 10), band);
        perfInsHelper.uploadAlbum("Смутное время");
        //By Mavrin
        band = uploadBand(new Person[]{berkut, mavrin},
                new LocalDate[]{LocalDate.of(1998, 1, 1),
                        LocalDate.of(1997, 1, 1)},
                new LocalDate[]{LocalDate.of(2000, 12, 31),
                        null}, "Маврин");
        c1 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Вольная птица",
                "Город, стоящий у Солнца",
                "Линия судьбы",
                "Воины"
        }, mavrin, mavrin);
        c2 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Откровение"}, pushkina, mavrin);
        perfInsHelper.init4album((Composition[]) ArrayUtils.addAll(c1, c2), LocalDate.of(2006, 1, 1), band);
        perfInsHelper.uploadAlbum("Откровение");
        //By Kipelov
        date1 = LocalDate.of(2002, 1, 1);
        band = uploadBand(new Person[]{kipelov, mavrin},
                new LocalDate[]{date1, date1},
                new LocalDate[]{null, LocalDate.of(2004, 12, 31),}, "Кипелов");
        perfInsHelper.init(new Composition[]{nightInJune}, band);
        perfInsHelper.markAsSingle(0, LocalDate.of(2009, 2, 19));
        perfInsHelper.uploadPerformances();
        perfInsHelper.init(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Мёртвая зона"}, pushkina, kipelov));
        perfInsHelper.markAsSingle(0, LocalDate.of(2013, 4, 8));
        perfInsHelper.uploadPerformances();
    }

    private void uploadRelated2AgataKristy(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person vadimSamoilov = new Person("Вадим", "Самойлов");
        Person glebSamoilov = new Person("Глеб", "Самойлов");
        Person karasev = new Person("Михаил", "Карасёв");
        uploadPersons(vadimSamoilov, glebSamoilov, karasev);
        //by Agata Kristy
        LocalDate date = LocalDate.of(1988, 1, 1);
        Band band = uploadBand(new Person[]{vadimSamoilov, glebSamoilov}, new LocalDate[]{date, date}, new LocalDate[]{null, null}, "Агата Кристи");
        Composition[] c1 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Viva Kalman!",
                "Пантера",
                "Герои",
                "Наша правда"
        }, vadimSamoilov, vadimSamoilov);
        Composition[] c2 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Сытая свинья",
                "Праздник семьи",
                "Бесса мэ…"
        }, glebSamoilov, glebSamoilov);
        perfInsHelper.init4album((Composition[]) ArrayUtils.addAll(c1, c2), LocalDate.of(1989, 1, 1), band);
        Collection album = perfInsHelper.uploadAlbum("Коварство и любовь");
        forCollection1.add(album.getPerformances().get(1));
        //With Bi-2 (Special case: single with two bands at the same time)
        Band bi2 = new Band("Би-2", Collections.EMPTY_MAP);
        appService.insertBand(bi2);
        c1 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Всё, как он сказал",}, karasev, karasev);
        Performance performance = new Performance(LocalDate.of(2007, 11, 16), c1[0], true, Arrays.asList(new Performer[]{band, bi2}));
        appService.insertPerformance(performance);
    }

    private void uploadRelated2SOAD(List<Performance> forCollection2, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person serj = new Person("Serj", "Tankian");
        Person daron = new Person("Daron", "Malakian");
        uploadPersons(serj, daron);
        //by Serj himself
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Empty Walls",
                "The Unthinking Majority",
                "Money",
                "Feed Us",
                "Saving Us",
                "Sky Is Over",
                "Baby",
                "Honking Antelope",
                "Lie Lie Lie",
                "Praise the Lord and Pass the Ammunition",
                "Beethoven's Cunt",
                "Elect the Dead"
        }, serj, serj), LocalDate.of(2007, 10, 22), serj);
        perfInsHelper.markAsSingle(0, LocalDate.of(2007, 9, 10));
        perfInsHelper.markAsSingle(8, LocalDate.of(2007, 12, 24));
        perfInsHelper.markAsSingle(5, LocalDate.of(2008, 1, 16));
        Collection album = perfInsHelper.uploadAlbum("Elect the Dead");
        forCollection2.add(album.getPerformances().get(9));
        forCollection2.add(album.getPerformances().get(11));
        //by SOAD
        LocalDate date = LocalDate.of(1994, 1, 1);
        Band band = uploadBand(new Person[]{serj, daron}, new LocalDate[]{date, date}, new LocalDate[]{null, null}, "System of a Down");
        Composition[] c1 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "X",
                "Forest",
                "Science",
                "Shimmy",
        }, serj, daron);
        Composition[] c2 = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Shimmy",
        }, serj, serj);
        perfInsHelper.init4album((Composition[]) ArrayUtils.addAll(c1, c2), LocalDate.of(2001, 9, 4), band);
        album = perfInsHelper.uploadAlbum("Toxicity");
        forCollection2.add(album.getPerformances().get(1));
    }

    private void uploadRelated2Lidemann(List<Performance> forCollection2, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person lidemann = new Person("Till", "Lindemann");
        Person peter = new Person("Peter", "Tägtgren");
        uploadPersons(lidemann, peter);
        //by Ramstein
        uploadBand(lidemann, LocalDate.of(1994, 1, 1), null, "Rammstein");
        //by Lindemann
        LocalDate date = LocalDate.of(2013, 1, 1);
        Band band = uploadBand(new Person[]{lidemann, peter}, new LocalDate[]{date, date}, new LocalDate[]{null, null}, "Lindemann");
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Skills in Pills",
                "Ladyboy",
                "Fat",
                "Fish On",
                "Children of the Sun",
                "Home Sweet Home",
                "Cowboy",
                "Golden Shower",
                "Yukon",
                "Praise Abort"
        }, lidemann, peter), LocalDate.of(2015, 06, 22), band);
        perfInsHelper.markAsSingle(9, LocalDate.of(2015, 05, 28));
        perfInsHelper.markAsSingle(3, LocalDate.of(2015, 10, 9));
        Collection album = perfInsHelper.uploadAlbum("Skills in Pills");
        forCollection2.add(album.getPerformances().get(3));
    }

    private void uploadRelated2Lagutenko(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person lagutenko = new Person("Илья", "Лагутенко");
        uploadPersons(lagutenko);
        //by Муми Тролль
        Band band = uploadBand(lagutenko, LocalDate.of(1983, 1, 1), null, "Муми Тролль");
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Карнавала.Нет",
                "Не Очень",
                "Скорее И Быстро",
                "Моя Певица",
                "Северный Полюс",
                "Невеста?",
                "Жабры",
                "Клубничная",
                "Сны",
                "Без Обмана",
                "Ему Не Взять Тебя",
                "Тише",
                "Случайности"
        }, lagutenko, lagutenko), LocalDate.of(2000, 2, 5), band);
        perfInsHelper.markAsSingle(5, LocalDate.of(1999, 11, 8));
        perfInsHelper.markAsSingle(0, LocalDate.of(1999, 12, 31));
        perfInsHelper.markAsSingle(9, LocalDate.of(2000, 5, 19));
        perfInsHelper.markAsSingle(3, LocalDate.of(2000, 12, 20));
        Collection album = perfInsHelper.uploadAlbum("Точно ртуть алоэ");
        forCollection1.add(album.getPerformances().get(5));
    }

    private void uploadRelated2Zemfira(List<Performance> forCollection1, PerformancesWithSamePerformersInsertionHelper perfInsHelper) {
        Person zemfira = new Person("Земфира", "Рамазанова");
        Person brown = new Person("David Arthur", "Brown");
        Person churikova = new Person("Инна", "Чурикова");
        uploadPersons(zemfira, brown, churikova);
        //by Zemfira herself
        perfInsHelper.init4album(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "В метро",
                "Воскресенье",
                "Дом",
                "Мы разбиваемся",
                "Мальчик",
                "Господа",
                "Я полюбила вас",
                "Возьми меня",
                "Снег начнётся",
                "1000 лет",
                "Во мне",
                "Спасибо [репетиция, ноябрь 2006]"
        }, zemfira, zemfira), LocalDate.of(2007, 10, 1), zemfira);
        perfInsHelper.markAsSingle(4, LocalDate.of(2007, 9, 1));
        perfInsHelper.markAsSingle(3, LocalDate.of(2007, 9, 4));
        perfInsHelper.markAsSingle(5, LocalDate.of(2008, 6, 9));
        perfInsHelper.markAsSingle(8, LocalDate.of(2008, 12, 1));
        Collection album = perfInsHelper.uploadAlbum("Спасибо");
        forCollection1.add(album.getPerformances().get(3));
        //With Churikova (Special case: single with two person at the same time)
        Composition[] compositions = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Не пошлое",}, zemfira, zemfira);
        Performance performance = new Performance(LocalDate.of(2004, 1, 1), compositions[0], true, Arrays.asList(new Performer[]{zemfira, churikova}));
        appService.insertPerformance(performance);
        //by The Uchpochmack
        Band band = uploadBand(zemfira, LocalDate.of(2013, 1, 1), null, "The Uchpochmack");
        perfInsHelper.init(uploadCompositionsWithTheSameAuthorAndComposer(new String[]{
                "Someday",
                "Lightbulbs",
        }, zemfira, zemfira), band);
        perfInsHelper.markAsSingle(0, LocalDate.of(2013, 11, 5));
        perfInsHelper.markAsSingle(1, LocalDate.of(2013, 11, 19));
        perfInsHelper.uploadPerformances();
        //With Brown (Special case: single with band and person at the same time)
        compositions = uploadCompositionsWithTheSameAuthorAndComposer(new String[]{"Mistress",}, zemfira, zemfira);
        performance = new Performance(LocalDate.of(2013, 11, 12), compositions[0], true, Arrays.asList(new Performer[]{brown, band}));
        appService.insertPerformance(performance);
    }


    private Band uploadBand(Person persons, LocalDate from, LocalDate to, String name) {
        HashMap<Person, List<DateRange>> map = new HashMap<>();
        map.put(persons, Arrays.asList(new DateRange[]{new DateRange(from, to)}));
        Band band = new Band(name, map);
        appService.insertBand(band);
        return band;
    }

    private Band uploadBand(Person[] persons, LocalDate[] from, LocalDate[] to, String name) {
        HashMap<Person, List<DateRange>> map = new HashMap<>();
        for (int i = 0; i < persons.length; i++) {
            map.put(persons[i], Arrays.asList(new DateRange[]{new DateRange(from[i], to[i])}));
        }
        Band band = new Band(name, map);
        appService.insertBand(band);
        return band;
    }

    private Composition[] uploadCompositionsWithTheSameAuthorAndComposer(String[] titles, Person author, Person composer) {
        Composition[] spasiboCompositions = new Composition[titles.length];
        for (int i = 0; i < spasiboCompositions.length; i++) {
            spasiboCompositions[i] = new Composition(titles[i], author, composer);
            appService.insertComposition(spasiboCompositions[i]);
        }
        return spasiboCompositions;
    }


}

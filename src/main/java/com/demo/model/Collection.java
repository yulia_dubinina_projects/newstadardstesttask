package com.demo.model;

import java.util.List;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class Collection extends DatabaseObject {
    protected String title;
    private List<Performance> performances;

    public Collection(String title, List<Performance> performances) {
        this.title = title;
        this.performances = performances;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public Collection(long id, String title, List<Performance> performances) {

        this.id = id;
        this.title = title;
        this.performances = performances;
    }

    @Override
    public String getDisplayString() {
        return "Collection \"" + title + "\"";
    }
}

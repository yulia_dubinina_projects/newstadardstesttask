package com.demo.model;

/**
 * Created by dhdaan on 13.06.2017.
 */
public abstract class DatabaseObject {
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public abstract String getDisplayString();

}

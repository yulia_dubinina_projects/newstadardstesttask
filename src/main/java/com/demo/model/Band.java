package com.demo.model;

import java.util.List;
import java.util.Map;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class Band extends Performer {
    private String name;
    private Map<Person, List<DateRange>> persons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Person, List<DateRange>> getPersons() {
        return persons;
    }

    public void setPersons(Map<Person, List<DateRange>> persons) {
        this.persons = persons;
    }

    public Band(String name, Map<Person, List<DateRange>> persons) {
        this.name = name;
        this.persons = persons;
    }

    public Band(long id, String name, Map<Person, List<DateRange>> persons) {
        this.id = id;
        this.name = name;
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Band{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public String getDisplayString() {
        return "Band \"" + name + "\"";
    }
}

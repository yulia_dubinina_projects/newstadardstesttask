package com.demo.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class Performance {
    private long id;
    private LocalDate date;
    private Composition composition;
    private boolean single;
    private List<Performer> performers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<Performer> getPerformers() {
        return performers;
    }

    public void setPerformers(List<Performer> performers) {
        this.performers = performers;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Composition getComposition() {
        return composition;
    }

    public void setComposition(Composition composition) {
        this.composition = composition;
    }

    public boolean isSingle() {
        return single;
    }

    public void setSingle(boolean single) {
        this.single = single;
    }

    public Performance(LocalDate date, Composition composition, boolean single, List<Performer> performers) {
        this.date = date;
        this.composition = composition;
        this.single = single;
        this.performers = performers;
   }

    public Performance(long id, LocalDate date, Composition composition, boolean single, List<Performer> performers) {
        this.id = id;
        this.date = date;
        this.composition = composition;
        this.single = single;
        this.performers = performers;
    }
}

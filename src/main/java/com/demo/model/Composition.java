package com.demo.model;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class Composition extends DatabaseObject {
    private String title;
    private Person author;
    private Person composer;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public Person getComposer() {
        return composer;
    }

    public void setComposer(Person composer) {
        this.composer = composer;
    }

    public Composition(String title, Person author, Person composer) {
        this.title = title;
        this.author = author;
        this.composer = composer;
    }

    public Composition(long id, String title, Person autor, Person composer) {

        this.id = id;
        this.title = title;
        this.author = autor;
        this.composer = composer;
    }

    @Override
    public String getDisplayString() {
        return "\"" + title + "\" (author: " + author.getDisplayString() + "; composer: " + composer.getDisplayString() + ")";
    }
}

package com.demo.model;

import java.util.List;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class Album extends Collection {
    private Performer performer;

    public Performer getPerformer() {
        return performer;
    }

    public void setPerformer(Performer performer) {
        this.performer = performer;
    }

    public Album(long id, String title, List<Performance> performances, Performer performer) {

        super(id, title, performances);
        this.performer = performer;
    }

    public Album(String title, List<Performance> performances, Performer performer) {

        super(title, performances);
        this.performer = performer;
    }


    @Override
    public String getDisplayString() {
        return "Album \"" + title + "\" by " + performer.getDisplayString();
    }

}

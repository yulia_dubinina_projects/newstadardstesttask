package com.demo.model;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by dhdaan on 12.06.2017.
 */
public class DateRange {
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public DateRange(LocalDate dateFrom, LocalDate dateTo) {

        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }
}

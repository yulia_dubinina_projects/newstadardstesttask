package com.demo.controller;


import com.demo.service.AppService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
public class AppController {
    @Autowired
    AppService appService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        //model.addAttribute("person", appService.findAll().get(0));
        return "home";
    }

    @RequestMapping(value = {"/first"}, method = RequestMethod.GET)
    public ModelAndView performersByComposition(ModelMap model,
                                                @RequestParam(value = "id", defaultValue = "-1") long id) {
        model.put("pageTitle", "Performers by composition");
        model.put("selectFromList", appService.getAllCompositions());
        if (id >= 0) {
            model.put("selectedId", id);
            model.put("resultList", appService.getPerformersByCompositionId(id));
        }
        return new ModelAndView("query", model);
    }

    @RequestMapping(value = {"/second"}, method = RequestMethod.GET)
    public ModelAndView bandByPerformer(ModelMap model,
                                          @RequestParam(value = "id", defaultValue = "-1") long id) {
        model.put("pageTitle", "Bands by performer");
        model.put("selectFromList", appService.getAllPersonPerformers());
        if (id >= 0) {
            model.put("selectedId", id);
            model.put("resultList", appService.getBandsByPersonId(id));
        }
        return new ModelAndView("query", model);
    }

    @RequestMapping(value = {"/third"}, method = RequestMethod.GET)
    public ModelAndView albumsByComposition(ModelMap model,
                                            @RequestParam(value = "id", defaultValue = "-1") long id) {
        model.put("pageTitle", "Albums by composition");
        model.put("selectFromList", appService.getAllCompositions());
        if (id >= 0) {
            model.put("selectedId", id);
            model.put("resultList", appService.getCollectionsByCompositionId(id));
        }
        return new ModelAndView("query", model);
    }

    @RequestMapping(value = {"/forth"}, method = RequestMethod.GET)
    public ModelAndView compositionsByComposer(ModelMap model,
                                               @RequestParam(value = "id", defaultValue = "-1") long id) {
        model.put("pageTitle", "Compositions by composer");
        model.put("selectFromList", appService.getAllComposers());
        if (id >= 0) {
            model.put("selectedId", id);
            model.put("resultList", appService.getCompositionsByComposerId(id));
        }
        return new ModelAndView("query", model);

    }

    @RequestMapping(value = {"/fifth"}, method = RequestMethod.GET)
    public ModelAndView compositionsByAuthor(ModelMap model,
                                             @RequestParam(value = "id", defaultValue = "-1") long id) {
        model.put("pageTitle", "Compositions by author");
        model.put("selectFromList", appService.getAllAuthors());
        if (id >= 0) {
            model.put("selectedId", id);
            model.put("resultList", appService.getCompositionsByAuthorId(id));
        }
        return new ModelAndView("query", model);
    }

    @RequestMapping(value = {"/sixth"}, method = RequestMethod.GET)
    public ModelAndView compositionsByPerformer(ModelMap model,
                                                @RequestParam(value = "id", defaultValue = "") String id) {
        model.put("pageTitle", "Compositions by performer");
        model.put("selectFromList", appService.getAllPerformers());
        if (!id.isEmpty()) {
            long num = Long.parseLong(StringUtils.substringBefore(id, "_"));
            String type = StringUtils.substringAfter(id, "_");
            model.put("selectedId", num);
            model.put("selectedType", type);
            model.put("resultList", appService.getCompositionsByPerformer(num, type));
        }else{
            model.put("selectedType", "undefined");
        }
        return new ModelAndView("query", model);
    }
}
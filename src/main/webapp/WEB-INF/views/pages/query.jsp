<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h2>${pageTitle}</h2>
<%--<tiles:insertAttribute name="query-content"/>--%>


<form action="${pageContext.request.contextPath}${requestScope['javax.servlet.forward.request_uri']}" method="get">
    <select name="id">
        <c:forEach items="${selectFromList}" var="selectItem">
            <c:set var="val" value="${selectItem.id}"/>
            <c:if test="${not empty selectedType}">
                <c:set var="val" value="${val}_${selectItem['class'].simpleName}"/>
            </c:if>
            <c:set var="sel" value=""/>
            <c:if test="${not empty selectedId && selectedId==selectItem.id}">
                <c:choose>
                    <c:when test="${not empty selectedType}">
                        <c:if test="${selectedType == selectItem['class'].simpleName}">
                            <c:set var="sel" value="selected"/>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:set var="sel" value="selected"/>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <option value="${val}" ${sel}>${selectItem.displayString}</option>
        </c:forEach>
    </select>
    <input type="submit" value="Go!"/>
</form>
<c:if test="${not empty selectedId}">
    <c:choose>
        <c:when test="${not empty resultList}">
            <c:forEach items="${resultList}" var="resultItem">
                ${resultItem.displayString}<br>
            </c:forEach>
        </c:when>
        <c:otherwise>
            Nothing was found...
        </c:otherwise>
    </c:choose>
</c:if>

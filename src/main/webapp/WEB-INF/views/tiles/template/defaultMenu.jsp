<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<nav>
    <c:set var="sel" value="${requestScope['javax.servlet.forward.request_uri']}"/>
    <c:choose>
        <c:when test="${not empty sel}">
            <c:if test="${fn:startsWith(sel, '/')}">
                <c:set var="sel" value="${fn:substring(sel, 1, fn:length(sel))}"/>
            </c:if>
            <c:if test="${empty sel}">
                <c:set var="sel" value="home"/>
            </c:if>
        </c:when>
        <c:otherwise>
            <c:set var="sel" value="home"/>
        </c:otherwise>
    </c:choose>
    <a href="${pageContext.request.contextPath}/">
        <img class="logo" src="${pageContext.request.contextPath}/static/img/manul.jpg">
    </a>
    <ul id="menu" class="${sel}">
        <li class="home"><a href="${pageContext.request.contextPath}/">Test task description</a></li>
        <li class="first"><a href="${pageContext.request.contextPath}/first">Performers by composition</a></li>
        <li class="second"><a href="${pageContext.request.contextPath}/second">Bands by performer</a></li>
        <li class="third"><a href="${pageContext.request.contextPath}/third">Albums by composition</a></li>
        <li class="forth"><a href="${pageContext.request.contextPath}/forth">Compositions by composer</a></li>
        <li class="fifth"><a href="${pageContext.request.contextPath}/fifth">Compositions by author</a></li>
        <li class="sixth"><a href="${pageContext.request.contextPath}/sixth">Compositions by performer</a></li>
    </ul>
</nav>